import sys

def main(args=None):
    if args is None:
        args = sys.argv[1:]

    print("Main Routine.")

    # Do argument parsing here (eg. with argparse) and your projects main logic

if __name__ == "__main__":
    main()