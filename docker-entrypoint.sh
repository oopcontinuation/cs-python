#!/bin/bash

# Exit script if any command fails
set -e

# Fetch contents of current working directory and perform word splitting on it
# If there is no content, the condition checks if ls doesn't return value
if ! [ "$(ls -A)" ]; then
  # Copy all the files from /var/www/sample (copied earlier in dockerfile)
  # Into current working directory, then set the permissions with chown
  echo >&2 "No files found in $PWD - copying sample files..."
  tar cf - --one-file-system -C /usr/src/app . | tar xf -
  echo >&2 "Complete! Sample files have been successfully copied to $PWD"
else
  # There are already files in current working directory
  echo >&2 "Files exist in volume, will not move sample files.."
fi

# Set permissions
chown -R py:py /usr/src/app

# Redirect input variables
exec "$@"