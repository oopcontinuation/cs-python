FROM python:3

COPY sample-project/ /usr/src/app

# Copy the entrypoint file, set permissions and create volume
COPY docker-entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh \
    && usermod -u 1001 py \
    && groupmod -g 1001 py
VOLUME /usr/src/app

ENTRYPOINT ["/entrypoint.sh"]
WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 8080

CMD python -m project